<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusStops extends Model
{
    //
	protected $table = "bus_stop";
	public $timestamps = false;
	protected $casts = [
		'lat' => 'float',
		'lng' => 'float',
	];
}
