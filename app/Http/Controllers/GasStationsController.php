<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GasStation;


class GasStationsController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
		$gasStations = GasStation::all();

		return response()->json( $gasStations );
	}

	public function syncCoords() {
		$mokymas = GasStation::all();

		foreach ( $mokymas as $istaiga ) {
			$response = \GoogleMaps::load( 'geocoding' )
								   ->setParam( [ 'address' => $istaiga->adresas ] )
								   ->get();

			$response     = json_decode( $response );
			$istaiga->lat = $response->results[0]->geometry->location->lat;
			$istaiga->lng = $response->results[0]->geometry->location->lng;

			$istaiga->save();


		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id ) {
		//
		//
		$gasStation                = GasStation::find( $id );
		$gasStation->seniunijos_id = $request->seniunijos_id;
		$gasStation->save();

		return response()->json( $request->seniunijos_id );
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ) {
		//
	}
}
