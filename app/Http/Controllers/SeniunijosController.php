<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Seniunijos;

class SeniunijosController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {

		$filtrai = [
			[
				"img" => "img/filtras/mokyklos.png",
				"title" => "Mokyklos",
				"color" => "#ffcb29"
			],
			[
				"img" => "img/filtras/degalines.png",
				"title" => "Degalines",
				"color" => "#c5d92d"
			],
			[
				"img" => "img/filtras/stoteles.png",
				"title" => "Stoteles",
				"color" => "#70b2e2"
			],
			[
				"img" => "img/filtras/bibliotekos.png",
				"title" => "Bibliotekos",
				"color" => "#ffcb29"
			],
			[
				"img" => "img/filtras/treniruokliai.png",
				"title" => "Treniruokliai",
				"color" => "#f1592b"
			],
			[
				"img" => "img/filtras/kultura.png",
				"title" => "Kultura",
				"color" => "#ffcb29"
			],
			[
				"img" => "img/filtras/parkai.png",
				"title" => "Parkai",
				"color" => "#41b655"
			],
			[
				"img" => "img/filtras/dviraciai.png",
				"title" => "Dviračiai",
				"color" => "#78909c"
			]

		];
		//
		$seniunijos = Seniunijos::all();

		return view( 'seniunijos.index', [
			'seniunijos' => $seniunijos,
			'filtrai' => $filtrai
		] );

	}

	public function compare( $id1, $id2 ) {

		$seniunija  = Seniunijos::find( 1 );
		$seniunija1 = Seniunijos::find( 2 );

		return view( 'seniunijos.compare', [
			'seniunija' => $seniunija,
			'seniunija1' => $seniunija1
		] );
	}

	public function getAllCoords( Request $request ) {
		$seniunijosKoordinates = Seniunijos::all();

		foreach ( $seniunijosKoordinates as $seniunija ) {
			if ( isset( $seniunija->coords ) ) {
				foreach ( $seniunija->coords as $coord ) {
				}
			}

		}

		return response()->json( $seniunijosKoordinates );
	}

	public function getCoords( Request $request, $id ) {
		$seniunijosKoordinates = Seniunijos::find( $id );

		foreach ( $seniunijosKoordinates->coords as $coord ) {

		}

		return response()->json( $seniunijosKoordinates->coords );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ) {
		//

		$filtrai = [
			[
				"img" => "img/filtras/mokyklos.png",
				"title" => "Mokyklos",
				"color" => "#ffcb29"
			],
			[
				"img" => "img/filtras/degalines.png",
				"title" => "Degalines",
				"color" => "#c5d92d"
			],
			[
				"img" => "img/filtras/stoteles.png",
				"title" => "Stoteles",
				"color" => "#70b2e2"
			],
			[
				"img" => "img/filtras/bibliotekos.png",
				"title" => "Bibliotekos",
				"color" => "#ffcb29"
			],
			[
				"img" => "img/filtras/treniruokliai.png",
				"title" => "Treniruokliai",
				"color" => "#f1592b"
			],
			[
				"img" => "img/filtras/kultura.png",
				"title" => "Kultura",
				"color" => "#ffcb29"
			],
			[
				"img" => "img/filtras/parkai.png",
				"title" => "Parkai",
				"color" => "#41b655"
			],
			[
				"img" => "img/filtras/dviraciai.png",
				"title" => "Dviračiai",
				"color" => "#78909c"
			]

		];

		$seniunija = Seniunijos::find( $id );

		return view( 'seniunijos.show', [ 'seniunija' => $seniunija, "filtrai" => $filtrai ] );
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {
		//
		$seniunija = Seniunijos::find( $id );

		return view( 'seniunijos.edit', [ 'seniunija' => $seniunija ] );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id ) {
		//
		$seniunija                     = Seniunijos::find( $id );
		$seniunija->pavadinimas        = $request->pavadinimas;
		$seniunija->gyventoju_skaicius = $request->gyventoju_skaicius;
		$seniunija->plotas             = $request->plotas;
		$seniunija->aprasymas          = $request->aprasymas;

		$seniunija->save();

		return redirect()->route( 'seniunijos.index' );
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ) {
		//
	}
}
