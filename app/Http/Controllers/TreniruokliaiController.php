<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Treniruokliai;

class TreniruokliaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		$treniruokliai = Treniruokliai::all();
//		$this->syncCoords();

		return response()->json( $treniruokliai );

    }

	public function syncCoords() {
		$mokymas = Treniruokliai::all();

		foreach ( $mokymas as $istaiga ) {
			$response = \GoogleMaps::load( 'geocoding' )
								   ->setParam( [ 'address' => $istaiga->adresas . " , Kaunas" ] )
								   ->get();

			$response     = json_decode( $response );
			$istaiga->lat = $response->results[0]->geometry->location->lat;
			$istaiga->lng = $response->results[0]->geometry->location->lng;

			$istaiga->save();


		}
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
		$treniruoklis = Treniruokliai::find( $id );
		$treniruoklis->seniunijos_id = $request->seniunijos_id;
		$treniruoklis->save();
		return response()->json( $request->seniunijos_id );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
