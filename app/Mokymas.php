<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mokymas extends Model
{
    //

	protected $table = "mokymas";
	public $timestamps = false;
}
