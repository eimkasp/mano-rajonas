<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seniunijos extends Model
{
    //

	public function coords() {
		return $this->hasMany('App\SeniunijuKoordinates', 'seniunijos_id', 'id');
	}

	public function svietimas() {
		return $this->hasMany('App\Mokymas', 'seniunijos_id', 'id');
	}

	public function bus_stops() {
		return $this->hasMany('App\BusStops', 'seniunijos_id', 'id');
	}

	public function gas_stations() {
		return $this->hasMany('App\GasStation', 'seniunijos_id', 'id');
	}


	public function treniruokliai() {
		return $this->hasMany('App\Treniruokliai', 'seniunijos_id', 'id');
	}

	public function konteineriai() {
		return $this->hasMany('App\Konteineriai', 'seniunijos_id', 'id');
	}

	public function street_art() {
		return $this->hasMany('App\StreetArt', 'seniunijos_id', 'id');
	}
}
