<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeniunijuKoordinates extends Model
{
    //

	protected $casts = [
		'lat' => 'float',
		'lng' => 'float',
	];

	protected $table = "sen_coord";
}
