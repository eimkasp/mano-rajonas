<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StreetArt extends Model
{
    //
	protected $table = "sek_street_art";
	public $timestamps = false;

}
