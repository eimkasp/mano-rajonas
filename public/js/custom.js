var map;
var rajonai = [];
var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';


jQuery(document).ready(function ($) {
  var data = {
    datasets: [{
      data: [90, 10],
      backgroundColor: [
        '#69ae44',
        'transparent',

      ],
      borderWidth: 0
    }],

  };
  if(document.getElementById('chart-area4')) {
    var ctx4 = document.getElementById('chart-area4').getContext('2d');
    var ctx5 = document.getElementById('chart-area5').getContext('2d');
    var ctx6 = document.getElementById('chart-area6').getContext('2d');

    var chart4 = new Chart(ctx4, {
      // The type of chart we want to create
      type: 'pie',
      // The data for our dataset
      data: data,
      // Configuration options go here
      options: {
        cutoutPercentage: 96
      }
    });

    var chart5 = new Chart(ctx5, {
      // The type of chart we want to create
      type: 'pie',
      // The data for our dataset
      data: data,
      // Configuration options go here
      options: {
        cutoutPercentage: 96
      }
    });

    var chart6 = new Chart(ctx6, {
      // The type of chart we want to create
      type: 'pie',
      // The data for our dataset
      data: data,
      // Configuration options go here
      options: {
        cutoutPercentage: 96
      }
    });
  }


  if(document.getElementById('chart-area')) {
    var ctx = document.getElementById('chart-area').getContext('2d');
    var ctx1 = document.getElementById('chart-area2').getContext('2d');
    var ctx2 = document.getElementById('chart-area3').getContext('2d');

    var chart = new Chart(ctx, {
      // The type of chart we want to create
      type: 'pie',
      // The data for our dataset
      data: data,
      // Configuration options go here
      options: {
        cutoutPercentage: 96
      }
    });

    var chart2 = new Chart(ctx1, {
      // The type of chart we want to create
      type: 'pie',
      // The data for our dataset
      data: data,
      // Configuration options go here
      options: {
        cutoutPercentage: 96
      }
    });

    var chart3 = new Chart(ctx2, {
      // The type of chart we want to create
      type: 'pie',
      // The data for our dataset
      data: data,
      // Configuration options go here
      options: {
        cutoutPercentage: 96
      }
    });
  }



  var firstID = 0;
  var secondID = 0;
  $(".seniunija a").click(function(e) {
    if(!$(this).hasClass('all')) {
      $(this).parent().addClass('active');
      console.log($(this).parent());
      if(firstID == 0) {
        firstID = $(this).data('sen');
      } else {
        secondID =  $(this).data('sen');
      }

      $("#pal").attr("href", "/seniunijos/compare/" + firstID + "/" + secondID);
      $("#one").attr("href", "/seniunijos/" + firstID);

      e.preventDefault();
      animateLanding();
      return false;
    }

  });

  function animateLanding() {

    $("#second").addClass('right-side-landing');
    $("#second-row").slideDown();
    $(".compare-buttons").fadeIn();
  }

  if(single == undefined) {
    single = false;
  }

  $('.count').each(function () {
    $(this).prop('Counter',0).animate({
      Counter: $(this).text()
    }, {
      duration: 4000,
      easing: 'swing',
      step: function (now) {
        $(this).text(Math.round(now * 10) / 10);
      }
    });
  });

  $('.count-big').each(function () {
    $(this).prop('Counter',0).animate({
      Counter: $(this).text()
    }, {
      duration: 4000,
      easing: 'swing',
      step: function (now) {
        $(this).text(Math.ceil(now));
      }
    });
  });

  function initMap() {
    if(single) {
      map = new google.maps.Map(document.getElementById('map'), {
        center: centras,
        mapTypeId: 'satellite',
        zoom: 13
      });
    } else {
      map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 54.929407, lng: 24.017053},
        mapTypeId: 'satellite',
        zoom: 13
      });
    }



    // Define the LatLng coordinates for the polygon's path.

  }

  initMap();
  setTimeout(function () {
    getCoords();
  }, 1000);

  setTimeout(function () {
    getSchools();
    getBusStops();
    getGasStations();
    getTraining();
  }, 3000);


  function getCoords() {
    $.ajax({
      method: "GET",
      url: APP_URL + "/seniunijos/get/all/coords",
      data: {}
    })
      .done(function (msg) {

        for (i = 0; i < msg.length; i++) {
          if (msg[i].coords.length > 0) {
            var triangleCoords = msg[i].coords;
            // Construct the polygon.
            rajonai.push(new google.maps.Polygon({
              paths: triangleCoords,
              strokeColor: '#FF0000',
              strokeOpacity: 0.8,
              strokeWeight: 2,
              fillColor: msg[i].spalva,
              fillOpacity: 0.35
            }));

            rajonai[i].setMap(map);
          }

        }

      });

  }


  function getBusStops() {
    var busMarkers = [];
    $.ajax({
      method: "GET",
      url: APP_URL +  "/stoteles",
      data: {}
    })
      .done(function (msg) {
          for (i = 0; i < msg.length; i++) {


            var lat = new google.maps.LatLng(msg[i].lng, msg[i].lat);

            //checkRajonasBus(lat, msg[i]);


            var marker = new google.maps.Marker({
              position: lat,
              icon: APP_URL + '/img/ikonos-05.png',
            });

            busMarkers.push(marker);

            var markerCluster = new MarkerClusterer(map, busMarkers,
              {
                imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
              });

          }

        }
      );
  }

  function getTraining() {
    var trainingMarkers = [];
    $.ajax({
      method: "GET",
      url: APP_URL + "/treniruokliai",
      data: {}
    })
      .done(function (msg) {
          for (i = 0; i < msg.length; i++) {


            var lat = new google.maps.LatLng(msg[i].lat, msg[i].lng);

            //checkRajonasBus(lat, msg[i]);


            var marker = new google.maps.Marker({
              position: lat,
              icon: APP_URL + '/img/ikonos-04.png',
            });

            trainingMarkers.push(marker);

            var markerCluster = new MarkerClusterer(map, trainingMarkers,
              {
                imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
              });

          }

        }
      );
  }


  function getSchools() {
    $.ajax({
      method: "GET",
      url: APP_URL + "/mokymas",
      data: {}
    })
      .done(function (msg) {
          for (i = 0; i < msg.length; i++) {
            var lat = new google.maps.LatLng(msg[i].lat, msg[i].lng);

            //checkRajonas(lat, msg[i]);

            var marker = new google.maps.Marker({
              position: lat,
              map: map,
              icon: APP_URL +  '/img/ikonos-02.png',
              title: 'Hello World!'
            });
          }

        }
      );
  }

  function getGasStations() {
    $.ajax({
      method: "GET",
      url: APP_URL +  "/koloneles",
      data: {}
    })
      .done(function (msg) {

          for (i = 0; i < msg.length; i++) {
            var lat = new google.maps.LatLng(msg[i].lat, msg[i].lng);

            //checkRajonasGasStation(lat, msg[i]);

            var marker = new google.maps.Marker({
              position: lat,
              map: map,
              icon: '/img/ikonos-03.png',
              title: 'Kolonele'
            });
          }

        }
      );
  }

  function checkRajonasGasStation(lat, mokykla) {
    for (var j = 0; j < rajonai.length; j++) {
      if (google.maps.geometry.poly.containsLocation(lat, rajonai[j])) {
        saveGasStationRajon(mokykla, j + 1);
      }

    }
  }

  function checkRajonas(lat, mokykla) {
    for (var j = 0; j < rajonai.length; j++) {
      if (google.maps.geometry.poly.containsLocation(lat, rajonai[j])) {
        saveSchool(mokykla, j + 1);
      }

    }
  }

  function checkRajonasBus(lat, mokykla) {
    for (var j = 0; j < rajonai.length; j++) {
      if (google.maps.geometry.poly.containsLocation(lat, rajonai[j])) {
        saveStopRajon(mokykla, j + 1);
      }
    }
  }

  function saveSchool(school, rajonas) {
    $.ajax({
      method: "PUT",
      url: APP_URL + "/mokymas/" + school.id,
      data: {id: school.id, seniunijos_id: rajonas}
    }).done(function (data) {
      console.log(data);
    })
  }

  function saveStopRajon(stop, rajonas) {
    $.ajax({
      method: "PUT",
      url: APP_URL + "/stoteles/" + stop.id,
      data: {id: stop.id, seniunijos_id: rajonas}
    }).done(function (data) {
      console.log(data);
    })
  }

  function saveGasStationRajon(stop, rajonas) {
    $.ajax({
      method: "PUT",
      url: APP_URL + "/koloneles/" + stop.id,
      data: {id: stop.id, seniunijos_id: rajonas}
    }).done(function (data) {
      console.log(data);
    })
  }


});