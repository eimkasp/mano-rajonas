<div  class="col-8 mt-3">
    <div class="row">
        <div class="col-md-12 headingas">
            <h3 class="text-white">Gero gyvenimo koeficientas <br> skirtingoms amžiaus grupėms  {{ $seniunija1->pavadinimas }}</h3>
        </div>
        <div class="col-md-4">
            <div class="chart">
                <canvas id="chart-area4" width="1140" height="570" style="display: block; height: 285px; width: 570px;"></canvas>
                <div class="chart-number count">
                    {{ sprintf('%f', $seniunija1->jaunimas) }}
                    {{--{{ $seniunija->jaunimas }}--}}
                </div>
                <div class="text-white text-desc">
                    Patrauklumas jaunimui
                </div>
            </div>

        </div>
        <div  class="col-md-4">
            <div class="chart">
                <canvas id="chart-area5" width="1140" height="570" style="display: block; height: 285px; width: 570px;"></canvas>
                <div class="chart-number count">
                    {{ $seniunija1->seimos }}
                </div>
                <div class="text-white text-desc">
                    Patrauklumas jaunoms šeimoms
                </div>
            </div>

        </div>
        <div class="col-md-4">
            <div class="chart">
                <canvas id="chart-area6" width="1140" height="570" style="display: block; height: 285px; width: 570px;"></canvas>
                <div class="chart-number count">
                    {{ $seniunija1->senijorai }}
                </div>
                <div class="text-white text-desc">
                    Patrauklumas senijorams
                </div>
            </div>

        </div>
    </div>
</div>