<div class="row">
    <div class="col-md-10 offset-md-1">
        <div class="row">
            <div class="col-md-10 offset-md-1 col-12">
                <h1>Sužinokite ir palyginkite gyvenimo kokybę įvariuose Kauno rajonuose</h1>
            </div>

            @foreach($seniunijos as $seniunija)
                <div class="col-md-3 col-6 seniunija">
                    <div class="seniunija-box">
                        <a href="{{ route('seniunijos.show', [$seniunija->id]) }}" data-sen="{{ $seniunija->id }}">
                            <div class="rajon-bg"
                                 style="background: url(/rajonai/{{ $seniunija->id }}.jpg); background-size: cover; background-position: bottom">

                            </div>
                            <div class="pavadinimas">
                                {{ $seniunija->pavadinimas }}
                            </div>
                        </a>
                    </div>

                </div>
            @endforeach
            <div class="col-md-3 col-6 seniunija">
                <div class="seniunija-box">
                    <a href="{{ route('seniunijos.index') }}" class="all">
                        <div class="rajon-bg"
                             style="background: url(/rajonai/landing.jpg); background-size: cover; background-position: bottom">

                        </div>
                        <div class="pavadinimas">
                            Miesto žemėlapis
                        </div>
                    </a>
                </div>

            </div>
        </div>
    </div>

</div>