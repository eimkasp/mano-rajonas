<div class="row" id="second-row">
    <div class="col-10 offset-1">
        <div class="row">
            <div class="col-10 offset-1">
                <h1 style="opacity: 0;">Sužinokite ir palyginkite gyvenimo kokybę įvariuose Kauno rajonuose</h1>

            </div>

            @foreach($seniunijos as $seniunija)
                <div class="col-md-3 seniunija">
                    <div class="seniunija-box">
                        <a href="{{ route('seniunijos.show', [$seniunija->id]) }}" data-sen="{{ $seniunija->id }}">
                            <div class="rajon-bg"
                                 style="background: url(/rajonai/{{ $seniunija->id }}.jpg); background-size: cover; background-position: bottom">

                            </div>
                            <div class="pavadinimas">
                                {{ $seniunija->pavadinimas }}
                            </div>
                        </a>
                    </div>

                </div>
            @endforeach
            <div class="col-md-3 seniunija">
                <div class="seniunija-box">
                    <a href="{{ route('seniunijos.index') }}">
                        <div class="rajon-bg"
                             style="background: url(/rajonai/landing.jpg); background-size: cover; background-position: bottom">

                        </div>
                        <div class="pavadinimas">
                            Miesto žemėlapis
                        </div>
                    </a>
                </div>

            </div>
        </div>
    </div>
</div>