@extends('layouts.app')

@section('content')
    <div class="container-fluid left-side-single single-rajon">
        <div class="row">
            <div class="col-md-6 right-side-landing py-3 content first-compare">
                <div class="compare-image" style="background: url('/rajonai/{{ $seniunija->id }}.jpg'); background-position: bottom center; background-size: cover;"></div>
                <div class="row rajon-stats">

                    <div class="col-md-8 offset-md-2 offset-2 col-8">
                        <h1>{{ $seniunija->pavadinimas }}</h1>

                        <div class="row stats-row">

                            <div class="col-md-4 col-6">
                                @if($seniunija->gyventoju_skaicius > $seniunija1->gyventoju_skaicius)
                                   <img class="compare-icon" src="/+.png">
                                @else
                                    <img class="compare-icon" src="/-.png">

                                @endif
                                <div class="number count">

                                    {{ $seniunija->gyventoju_skaicius }}
                                </div>
                                <span class="title">Gyventojų skaičius</span>
                            </div>


                            <div class="col-md-4 col-6">
                                @if($seniunija->svietimas > $seniunija1->svietimas)
                                    <img class="compare-icon" src="/+.png">
                                @else
                                    <img class="compare-icon" src="/-.png">

                                @endif
                                <div class="number count">
                                    {{ count($seniunija->svietimas) }}
                                </div>
                                <span class="title">Švietimo įstaigų</span>
                            </div>

                            <div class="col-md-4 col-6">
                                @if($seniunija->bus_stops > $seniunija1->bus_stops)
                                    <img class="compare-icon" src="/+.png">
                                @else
                                    <img class="compare-icon" src="/-.png">
                                @endif
                                <div class="number count">
                                    {{ count($seniunija->bus_stops) }}
                                </div>
                                <span class="title">Autobusų stotelių</span>
                            </div>

                            <div class="col-md-4 col-6">
                                @if($seniunija->gas_stations > $seniunija1->gas_stations)
                                    <img class="compare-icon" src="/+.png">
                                @else
                                    <img class="compare-icon" src="/-.png">
                                @endif
                                <div class="number count">
                                    {{ count($seniunija->gas_stations) }}
                                </div>
                                <span class="title">Degalinių</span>
                            </div>

                            <div class="col-md-4 col-6">
                                @if($seniunija->treniruokliai > $seniunija1->treniruokliai)
                                    <img class="compare-icon" src="/+.png">
                                @else
                                    <img class="compare-icon" src="/-.png">
                                @endif
                                <div class="number count">
                                    {{ count($seniunija->treniruokliai) }}
                                </div>
                                <span class="title">Treniruoklių</span>
                            </div>

                            <div class="col-md-4 col-6">
                                @if($seniunija->konteineriai > $seniunija1->konteineriai)
                                    <img class="compare-icon" src="/+.png">
                                @else
                                    <img class="compare-icon" src="/-.png">
                                @endif
                                <div class="number count">
                                    {{ count($seniunija->konteineriai) }}
                                </div>
                                <span class="title">Konteinerių</span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-md-6 right-side-landing py-3 content first-compare">
                <div class="compare-image" style="background: url('/rajonai/{{ $seniunija1->id }}.jpg'); background-position: bottom center; background-size: cover;"></div>
                <div class="row rajon-stats">
                    <div class="col-md-8 offset-md-2 offset-2 col-8">
                        <h1>{{ $seniunija1->pavadinimas }}</h1>

                        <div class="row stats-row">
                            <div class="col-md-4 col-6">
                                @if($seniunija->gyventoju_skaicius < $seniunija1->gyventoju_skaicius)
                                    <img class="compare-icon" src="/+.png">
                                @else
                                    <img class="compare-icon" src="/-.png">
                                @endif
                                <div class="number count">
                                    {{ $seniunija1->gyventoju_skaicius }}
                                </div>
                                <span class="title">Gyventojų skaičius</span>
                            </div>


                            <div class="col-md-4 col-6">
                                @if($seniunija->svietimas < $seniunija1->svietimas)
                                    <img class="compare-icon" src="/+.png">
                                @else
                                    <img class="compare-icon" src="/-.png">
                                @endif
                                <div class="number count">
                                    {{ count($seniunija1->svietimas) }}
                                </div>
                                <span class="title">Švietimo įstaigų</span>
                            </div>

                            <div class="col-md-4 col-6">
                                @if($seniunija->bus_stops < $seniunija1->bus_stops)
                                    <img class="compare-icon" src="/+.png">
                                @else
                                    <img class="compare-icon" src="/-.png">
                                @endif
                                <div class="number count">
                                    {{ count($seniunija1->bus_stops) }}
                                </div>
                                <span class="title">Autobusų stotelių</span>
                            </div>

                            <div class="col-md-4 col-6">
                                @if($seniunija->gas_stations < $seniunija1->gas_stations)
                                    <img class="compare-icon" src="/+.png">
                                @else
                                    <img class="compare-icon" src="/-.png">
                                @endif
                                <div class="number count">
                                    {{ count($seniunija1->gas_stations) }}
                                </div>
                                <span class="title">Degalinių</span>
                            </div>

                            <div class="col-md-4 col-6">
                                @if($seniunija->treniruokliai < $seniunija1->treniruokliai)
                                    <img class="compare-icon" src="/+.png">
                                @else
                                    <img class="compare-icon" src="/-.png">
                                @endif
                                <div class="number count">
                                    {{ count($seniunija1->treniruokliai) }}
                                </div>
                                <span class="title">Treniruoklių</span>
                            </div>

                            <div class="col-md-4 col-6">
                                @if($seniunija->konteineriai < $seniunija1->konteineriai)
                                    <img class="compare-icon" src="/+.png">
                                @else
                                    <img class="compare-icon" src="/-.png">
                                @endif
                                <div class="number count">
                                    {{ count($seniunija1->konteineriai) }}
                                </div>
                                <span class="title">Konteinerių</span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>
    <div class="container-fluid general-stats compare-charts">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    @include('chart')
                </div>
                <div class="col-md-6">
                    @include('chart2')
                </div>
            </div>


        </div>

    </div>
    <div class="container-fluid">
        <div class="row">


        </div>
    </div>

    <div class="map-wrapper">
        <div id="map"></div>
    </div>
    <script>
      var single = true;
      var centras = { lat: {{ $seniunija->svietimas[0]->lat }}, lng: {{ $seniunija->svietimas[0]->lng }} }
    </script>
@endsection