@extends('layouts.app')

@section('content')
    <div class="container py-4">


        <h1>Redaguojate: {{ $seniunija->pavadinimas }}</h1>
        <form action="{{ route('seniunijos.update', [$seniunija->id]) }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('put') }}
            <div>
                <input type="text" name="pavadinimas" class="form-control" placeholder="Pavadinimas" value="{{ $seniunija->pavadinimas }}">
            </div>

            <div>
                <input type="file" name="nuotrauka" class="form-control">
            </div>

            <div>
                <input type="text" name="gyventoju_skaicius" class="form-control" placeholder="Gyventoju skaicius">
            </div>

            <div>
                <input type="text" name="plotas" class="form-control" placeholder="plotas">
            </div>

            <textarea class="form-control" name="aprasymas"></textarea>

            <input type="submit" class="btn btn-success">
        </form>

    </div>

@endsection