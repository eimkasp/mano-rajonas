@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">

            @foreach($filtrai as $a)
                <div class="filtras" style="background: {{ $a['color'] }}">
                    <div class="mt-3">
                        <img src="{{ asset($a['img']) }}">
                        <div class="text-center">
                            {{ $a['title'] }}
                        </div>
                    </div>

                </div>
            @endforeach
        </div>
    </div>
    <div class="container-fluid">

        <div class="map-wrapper">
            <div id="map"></div>
        </div>
        <table class="table">
            <tr>
                <th>Rajonas</th>
                <th>Svietimo istaigos</th>
                <th>Stoteles</th>
                <th>Koloneles</th>
                <th>Treniruokliu kiekis</th>
                <th>Konteineriu kiekis</th>
            </tr>
            @foreach($seniunijos as $seniunija)
                <tr>
                    <td>
                        {{ $seniunija->pavadinimas }}
                    </td>
                    <td>
                        {{ count($seniunija->svietimas) }}
                    </td>
                    <td>
                        {{ count($seniunija->bus_stops) }}
                    </td>
                    <td>
                        {{ count($seniunija->gas_stations) }}
                    </td>
                    <td>
                        {{ count($seniunija->treniruokliai) }}
                    </td>
                    <td>
                        {{ count($seniunija->konteineriai) }}
                    </td>
                   {{-- <td>
                        <a href="{{ route('seniunijos.edit', [$seniunija->id]) }}">Redaguoti</a>
                    </td>--}}
                </tr>
            @endforeach
        </table>
    </div>


    <script>

    </script>

@endsection

