@extends('layouts.app')

@section('content')
    <div class="container-fluid left-side-single single-rajon" style="background: url(/rajonai/{{ $seniunija->id }}.jpg); background-size: cover; background-position: bottom">
        <div class="row">
            <div class="col-md-12 right-side-landing py-3 content">
                <h1>{{ $seniunija->pavadinimas }}</h1>
                <div class="row rajon-stats">
                    <div class="col-md-8 offset-md-2 offset-2 col-8">
                        <div class="row">
                            <div class="col-md-4 col-6">
                                <div class="number count">
                                    {{ $seniunija->gyventoju_skaicius }}
                                </div>
                                <span class="title">Gyventojų skaičius</span>
                            </div>


                            <div class="col-md-4 col-6">
                                <div class="number count">
                                    {{ count($seniunija->svietimas) }}
                                </div>
                                <span class="title">Švietimo įstaigų</span>
                            </div>

                            <div class="col-md-4 col-6">
                                <div class="number count">
                                    {{ count($seniunija->bus_stops) }}
                                </div>
                                <span class="title">Autobusų stotelių</span>
                            </div>

                            <div class="col-md-4 col-6">
                                <div class="number count">
                                    {{ count($seniunija->gas_stations) }}
                                </div>
                                <span class="title">Degalinių</span>
                            </div>

                            <div class="col-md-4 col-6">
                                <div class="number count">
                                    {{ count($seniunija->treniruokliai) }}
                                </div>
                                <span class="title">Treniruoklių</span>
                            </div>

                            <div class="col-md-4 col-6">
                                <div class="number count">
                                    {{ count($seniunija->konteineriai) }}
                                </div>
                                <span class="title">Konteinerių</span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>
    <div class="container-fluid general-stats">
        <div class="row">
            <div class="col-4 number-hold">
                <div class="big-number count-big">
                    {{ round($seniunija->bendras, 1) }}
                </div>
                <div class="big-desc">

                    Bendras gero gyvenimo koeficientas {{ $seniunija->pavadinimas }}
                </div>
            </div>
           <div  class="col-8 mt-3">
               <div class="row">
                   <div class="col-md-12 headingas">
                       <h3 class="text-white">Gero gyvenimo koeficientas <br> skirtingoms amžiaus grupėms  {{ $seniunija->pavadinimas }}</h3>
                   </div>
                   <div class="col-md-4">
                       <div class="chart">
                           <canvas id="chart-area" width="1140" height="570" style="display: block; height: 285px; width: 570px;"></canvas>
                           <div class="chart-number count">
                               {{ sprintf('%f', $seniunija->jaunimas) }}
                               {{--{{ $seniunija->jaunimas }}--}}
                           </div>
                           <div class="text-white text-desc">
                               Patrauklumas jaunimui
                           </div>
                       </div>

                   </div>
                   <div  class="col-md-4">
                       <div class="chart">
                           <canvas id="chart-area2" width="1140" height="570" style="display: block; height: 285px; width: 570px;"></canvas>
                           <div class="chart-number count">
                               {{ $seniunija->seimos }}
                           </div>
                           <div class="text-white text-desc">
                               Patrauklumas jaunoms šeimoms
                           </div>
                       </div>

                   </div>
                   <div class="col-md-4">
                       <div class="chart">
                           <canvas id="chart-area3" width="1140" height="570" style="display: block; height: 285px; width: 570px;"></canvas>
                           <div class="chart-number count">
                               {{ $seniunija->senijorai }}
                           </div>
                           <div class="text-white text-desc">
                               Patrauklumas senijorams
                           </div>
                       </div>

                   </div>
               </div>
           </div>



        </div>
    </div>
    <div class="container-fluid">
        <div class="row">

            @foreach($filtrai as $a)
                <div class="filtras" style="background: {{ $a['color'] }}">
                    <div class="mt-3">
                        <img src="{{ asset($a['img']) }}">
                        <div class="text-center">
                            {{ $a['title'] }}
                        </div>
                    </div>

                </div>
            @endforeach
        </div>
    </div>
    <div class="map-wrapper">
        <div id="map"></div>
    </div>
    <script>
        var single = true;
        var centras = { lat: {{ $seniunija->svietimas[0]->lat }}, lng: {{ $seniunija->svietimas[0]->lng }} }
    </script>
@endsection