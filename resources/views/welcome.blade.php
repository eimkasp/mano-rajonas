@extends('layouts.app')

@section('content')
    <div class="container-fluid left-side-landing">
        <div class="row">
            <div class="compare-buttons">
                <a href="#"  id="pal" class="btn btn-success">Palyginti</a>
                <a href="#" id="one" class="btn btn-success">Peržiūrėti pasirinktą rajona</a>
            </div>
            <div class="col-md-6" id="second">
                @include('second')
            </div>
            <div class="col-md-6 right-side-landing py-3">
                @include('first')
            </div>


        </div>
    </div>
@endsection