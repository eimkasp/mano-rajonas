<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Seniunijos;


Route::get('/', function () {
	$seniunijos = Seniunijos::all();

    return view('welcome', ['seniunijos'=> $seniunijos]);
});
Route::get('/apie', function() {
	return view('about');
})->name('apie');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('seniunijos', 'SeniunijosController');
Route::get('seniunijos/compare/{id1}/{id2}', 'SeniunijosController@compare')->name('compare');
Route::resource('mokymas', 'MokymasController');
Route::resource('stoteles', 'BusController');
Route::resource('street-art', 'StreetArtController');
Route::resource('library', 'LibraryController');
Route::resource('park', 'ParkController');
Route::resource('koloneles', 'GasStationsController');
Route::resource('treniruokliai', 'TreniruokliaiController');
Route::resource('konteineriai', 'KonteineriaiController');
Route::get('seniunijos/{id}/coords', 'SeniunijosController@getCoords');
Route::get('seniunijos/get/all/coords', 'SeniunijosController@getAllCoords');
